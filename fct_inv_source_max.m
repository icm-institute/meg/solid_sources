function fct_inv_source(ijob)
% suj, n_tris, source
% for all sources of the the superhigh resolution mesh, take nsubj, ntri,
% compute a simple, inject signal, simple t-test, store result value

%suj représente une matrice avec un certain nombre de sujets pris au hasard
%tri représente une matrice avec nombre d'essais pris au hasard
%source représente une matrice avec des sources prises au hasard

% preparing environment
% codedir = '/network/lustre/iss02/cenir/analyse/meeg/SOLID_SOURCES/jobs/00_inv_sources/amp_10/scripts';
codedir = '/DATA_FAST/DATA/SOLID_SOURCES/code';
addpath(codedir)
p_setpath

load('../../common.mat','allctxsm','alldata','allgrad');
load('../clusterjob_common.mat','fs','specif')
[fs.ctxsm] = rep2struct(allctxsm);
[fs.grad] = rep2struct(allgrad);

struct2ws(specif)

load(sprintf('../clusterjob%03d.mat',ijob),'MCs')

H = false([size(MCs) size(fs(1).ctxsm.pos,1)]);
%% start looping through sources
for i_suj = 1:size(suj)
    for i_s = 1:size(source)
        for i_ntri = 1:numel(n_tris)
            n_tri = n_tris(i_ntri);
            %         tic
            if isfield(fs,'lf')  %??
                fs = rmfield(fs,'lf');
            end
            %% load precomputed leadfield 
            load(fullfile(datadir,'ctxsm_all_lf_32k/','Native_32k_fs_LR',num2str(floor(i_s/1000),'%03d'),['lf_' num2str(i_s,'%06d')]),'lf');
            % now for each subject
            % we stack leadfields in the fs structure
                % mark dipole position
                data = [];
                data.label = fs(i_suj).label;
                data.grad = fs(i_suj).grad;
                
                fs(i_suj).dippos = fs(i_suj).ctxsm.pos(i_s,:);
                fs(i_suj).dipmom = fs(i_suj).ctxsm.nrm(i_s,:)';
                fs(i_suj).dipmom = fs(i_suj).dipmom ./ norm(fs(i_suj).dipmom);
                fs(i_suj).lf = leadfield.leadfield{i_s}(chnb(fs(i_suj).label,leadfield.label),:);

                lftopo1 = fs(i_suj).lf * fs(i_suj).dipmom * amp;
                
                for i_tri = 1:n_tri
                    data.trial{i_tri} = alldata(:, 1:n_pts, i_tri, i_suj);
                    data.trial{i_tri}(:, 1:n_pts) = data.trial{i_tri}(:, 1:n_pts) + lftopo1 ;
                    data.time{i_tri} = linspace(t_min, t_max, n_pts);
                end

                   
                data.dimord = 'chan_time';
               

                cfg = [];
                cfg.covariance = 'yes';
                cfg.covariancewindow = [-.2 1]; %it will calculate the covariance matrix
                % on the timepoints that are
                % before the zero-time point in the
                % trials
                tlck = ft_timelockanalysis(cfg, data); %que mettre pour data?
                
                 %%
                %inverse model


                cfg               = [];
                cfg.method        = 'mne';
                cfg.sourcemodel   = leadfield;
                cfg.headmodel     = fs(i_suj).headmodel;
                cfg.mne.prewhiten = 'yes';
                cfg.mne.lambda    = 3;
                cfg.mne.scalesourcecov = 'yes';

                source1{i_suj,i_tri}          = ft_sourceanalysis(cfg,tlck);
%%
              
        end
    end
end

MCs = rmfield(MCs,{'m','h','p','tstat','df','sd'});

% save data in a .mat file to be read by the next plotting script
save(sprintf('../clusterjob%03d.mat',ijob),'MCs','H')
