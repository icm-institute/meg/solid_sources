
p_setpath
load layout
%%
fs = p_dbload('restin',{'name','label','headmodel','ds','ctxsm','grad','hm','ctxsm_inflated'});
load(fullfile(datadir,'alldata_50timepoints.mat'))
alldata = reshape(alldata, [248,50*200,89]);
%%
alldata = reshape(alldata, [248, 200, 50, 89]);
lfdir = fullfile(datadir,'lf_32k');
amp = 10;
t_min = -.2;
t_max = 1;
n_pts = 200;
n_source = 1; %pb

i_suj = 2;
%i_s = 1800; %1:size(fs(1).ctxsm.pos,1)
%redisp(i_s)
 n_tris = [2 5 7 10 12 15 20 25 30 35 40 45 50];
%n_tris = [2 10 20 50];

DLE = NaN(n_source,numel(n_tris));
OA = NaN(n_source,numel(n_tris));
SD = NaN(n_source,numel(n_tris));
%%
      % load le leadfield precomputé pour un sujet au hasard
      load(fullfile(lfdir,['lf_32k_' fs(i_suj).ds '.mat']))
for i_s = 658 %[487 658 246] 
    for i_ntri = 1:numel(n_tris)
      n_tri = n_tris(i_ntri);
      %%
      % créer une data structure de données "timelocked"

      data = [];
      data.label = fs(i_suj).label;
      data.grad = fs(i_suj).grad;
      fs(i_suj).dippos = fs(i_suj).ctxsm.pos(i_s,:);
      fs(i_suj).dipmom = fs(i_suj).ctxsm.nrm(i_s,:)';
      fs(i_suj).dipmom = fs(i_suj).dipmom ./ norm(fs(i_suj).dipmom);
      % take leadfield
      fs(i_suj).lf = leadfield.leadfield{i_s}(chnb(fs(i_suj).label,leadfield.label),:);


      lftopo1 = fs(i_suj).lf * fs(i_suj).dipmom * amp;

      for i_tri = 1:n_tri


        data.trial{i_tri} = alldata(:, 1:n_pts, i_tri, i_suj);
        data.trial{i_tri}(:, 1:n_pts) = data.trial{i_tri}(:, 1:n_pts) + lftopo1 ;
        data.time{i_tri} = linspace(t_min, t_max, n_pts);    %f_e=508.6275Hz


      end


      data.dimord = 'chan_time';
      %data.avg = lftopo1;

      cfg = [];
      cfg.covariance = 'yes';
      cfg.covariancewindow = [-.2 1]; %it will calculate the covariance matrix
      % on the timepoints that are
      % before the zero-time point in the
      % trials
      tlck = ft_timelockanalysis(cfg, data);

      %%
      %inverse model


      cfg               = [];
      cfg.method        = 'mne';
      cfg.sourcemodel   = leadfield;
      cfg.headmodel     = fs(i_suj).headmodel;
      cfg.mne.prewhiten = 'yes';
      cfg.mne.lambda    = 3;
      cfg.mne.scalesourcecov = 'yes';

      source1          = ft_sourceanalysis(cfg,tlck);


      %%
      %statistiques

      DLE(i_ntri) = f_dle(source1.pos,source1.avg.pow, i_s);    %pb
      OA(i_ntri) = f_oa(source1.pos,source1.avg.pow, i_s);
      SD(i_ntri) = f_sd(source1.pos,source1.avg.pow, i_s);
      
     
      
    end


end

%%
%stat

% DLE_mean = mean(DLE);
% OA_mean = mean(OA);
% SD_mean = mean(SD);
% 
% DLE_std_h = DLE_mean + std(DLE);
% DLE_std_l = DLE_mean - std(DLE);
% 
% OA_std_h = DLE_mean + std(DLE);
% OA_std_l = DLE_mean - std(DLE);
% 
% SD_std_h = DLE_mean + std(DLE);
% Sd_std_l = DLE_mean - std(DLE);




%%
%plot

figure(2)
subplot(2,2,1)
plot(n_tris,DLE);
axis([0 50 0 0.045]);
xlabel('Number of trials');
ylabel('DLE');
subplot(2,2,2)
plot(n_tris,SD);
axis([0 50 0 0.3]);
xlabel('Number of trials');
ylabel('SD');

%n_tris,DLE_std_h,'--',n_tris,DLE_std_h,'--'



