
p_setpath_loc

fs = p_dbload('restin',{'name','label','ds'});

alllf = flister('lf_32k_(?<ds>\d{6}).mat','dir',fullfile(datadir,'lf_32k'));
mymkdir(fullfile(datadir,'lf_32k_reorg'))

for i = 1:numel(alllf)
    load(alllf(i).name)
        isuj = regexpcell({fs.ds},alllf(i).ds);
        targetlabel = fs(isuj).label;
        herelabel = leadfield.label;
        nuord = chnb(targetlabel, herelabel);
    for i_s = 1:numel(leadfield.leadfield)
        if isempty(leadfield.leadfield{i_s})
            continue
        end
        leadfield.leadfield{i_s} = leadfield.leadfield{i_s}(nuord,:);
    end
    inside = leadfield.inside;
    lf = leadfield.leadfield;
    save(strrep(alllf(i).name,'/lf_32k/','/lf_32k_reorg/'),'lf', 'inside')
end
