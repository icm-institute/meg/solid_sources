p_setpath
%%load variables of interest from clusterjob files into workspace


%load all .mat files starting with clusterjob
%clusterjob_dir = '/DATA_FAST/DATA/SOLID_SOURCES/jobs/00_new_try/amp_10/';
clusterjob_dir = '/network/lustre/iss02/cenir/analyse/meeg/SOLID_SOURCES/jobs/good_data/source_246/';
clusterjob_pattern = fullfile(clusterjob_dir, 'clusterjob*.mat');

clusterjob_files = dir(clusterjob_pattern);
%%
%delete last clusterjob file which corresponds to clusterjob_common file
%clusterjob_files(regexpcell({clusterjob_files.name},'common')) = [];
i_clusterjob_common = numel(clusterjob_files);
clusterjob_files(i_clusterjob_common)=[];
%%
%extract variables from .mat files
for k = 1:length(clusterjob_files)
  clusterjob_filename = fullfile(clusterjob_dir, clusterjob_files(k).name);
  clusterjob_data = load(clusterjob_filename); % Retrieves a structure.
  DLE_raw(k,:) = clusterjob_data.DLE;
  SD_raw(k,:) = clusterjob_data.SD;
end


MCs_raw = clusterjob_data.MCs;

%trouver nsujs
for i = 1:size(MCs_raw,2)
    numsuj = size(MCs_raw(i, 1).sujs,2);
    nsujs(i) = numsuj;
end

%trouver ntri
for j = 1:size(MCs_raw,1)
    numtri = size(MCs_raw(1, j).tris,2);
    ntris(j) = numtri;
end

nnsujs = numel(nsujs);
nntris = numel(ntris);

%%
%reshape

DLE_final = reshape(DLE_raw',[6,6,500]);
SD_final = reshape(SD_raw',[6,6,500]);
%%
%stat

DLE_mean = mean(DLE_final,3);
SD_mean = mean(SD_final,3);

DLE_std_h = DLE_mean + std(DLE_final);
DLE_std_l = DLE_mean - std(DLE_final);
DLE_Q = quantile(DLE_final,3);

SD_std_h = SD_mean + std(SD_final);
SD_std_l = SD_mean - std(SD_final);
SD_Q = quantile(SD_final,3);

[min_DLE i_min_DLE] = min(DLE_final);
[min_SD i_min_SD] = min(SD_final);
[max_DLE i_max_DLE] = max(DLE_final);
[max_SD i_max_SD] = max(SD_final);

%% 
%plot DLE

figure(1)
DLE_mean_form=flip(DLE_mean'); 
set(axes,'FontSize',10);%,'position',[0.4686    0.10137    0.4059    0.2958]);%[0.5200    0.0619    0.3700    0.2700])%
colormap(gca,varycolor(256,'Moreland_Black_Body'))
hold on

imagesc(nsujs,ntris,DLE_mean',[0.05 0.10])%,[0.03 0.13]
levs = [.075 .08 .1];
[xq,yq] = meshgrid(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)));

nutoplot = interp2(nsujs,ntris,DLE_mean',xq,yq,'spline');
contour(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)),nutoplot,levs,'k','showtext','on')
set(gca,'layer','top')
box on
axis xy
xlabel('Number of subjects')
xticks(nsujs)
xticklabels(nsujs);
ylabel('Number of trials')
yticks(ntris)
yticklabels(ntris);%num2str(cellstr2num(get(gca,'yticklabel'))/2)
set(gca,'tag','power_contour')
h = colorbar;%('position',[0.9076    0.1037    0.0196    0.2958]);%,[0.9116    0.0619    0.0196    0.27]);
h.Label.String = 'DLE';
h.Label.FontSize =12;

%set(gcf,'name',[num2str(i_ar,'%02d') ' ' strrep(all_areas.parcels{i_ar},'L_L_','')]);

%%
%histogramme

fig = figure(2);
clf

%2 trial
subplot(6,6,1);
histogram(DLE_final(1,1,:),100); %2 sujets
%axis off;
subplot(6,6,2);
histogram(DLE_final(2,1,:),100); %5 sujets
%axis off;
subplot(6,6,3);
histogram(DLE_final(3,1,:),100);
%axis off;
subplot(6,6,4);
histogram(DLE_final(4,1,:),100);
%axis off;
subplot(6,6,5);
histogram(DLE_final(5,1,:),100);
%axis off;
subplot(6,6,6);
histogram(DLE_final(6,1,:),100);
%axis off;

%5 trial
subplot(6,6,7);
histogram(DLE_final(1,2,:),100);
%axis off;
subplot(6,6,8);
histogram(DLE_final(2,2,:),100);
%axis off;
subplot(6,6,9);
histogram(DLE_final(3,2,:),100);
%axis off;
subplot(6,6,10);
histogram(DLE_final(4,2,:),100);
%axis off;
subplot(6,6,11);
histogram(DLE_final(5,2,:),100);
%axis off;
subplot(6,6,12);
histogram(DLE_final(6,2,:),100);
%axis off;

%10 tri
subplot(6,6,13);
histogram(DLE_final(1,3,:),100);
% axis off;
subplot(6,6,14);
histogram(DLE_final(2,3,:),100);
% axis off;
subplot(6,6,15);
histogram(DLE_final(3,3,:),100);
% axis off;
subplot(6,6,16);
histogram(DLE_final(4,3,:),100);
% axis off;
subplot(6,6,17);
histogram(DLE_final(5,3,:),100);
% axis off;
subplot(6,6,18);
histogram(DLE_final(6,3,:),100);
% axis off;

%20 tri
subplot(6,6,19);
histogram(DLE_final(1,4,:),100);
% axis off;
subplot(6,6,20);
histogram(DLE_final(2,4,:),100);
% axis off;
subplot(6,6,21);
histogram(DLE_final(3,4,:),100);
% axis off;
subplot(6,6,22);
histogram(DLE_final(4,4,:),100);
% axis off;
subplot(6,6,23);
histogram(DLE_final(5,4,:),100);
% axis off;
subplot(6,6,24);
histogram(DLE_final(6,4,:),100);
% axis off;

subplot(6,6,25);
histogram(DLE_final(1,5,:),100);
% axis off;
subplot(6,6,26);
histogram(DLE_final(2,5,:),100);
% axis off;
subplot(6,6,27);
histogram(DLE_final(3,5,:),100);
% axis off;
subplot(6,6,28);
histogram(DLE_final(4,5,:),100);
% axis off;
subplot(6,6,29);
histogram(DLE_final(5,5,:),100);
% axis off;
subplot(6,6,30);
histogram(DLE_final(6,5,:),100);
% axis off;

subplot(6,6,31);
histogram(DLE_final(1,6,:),100);
% axis off;
subplot(6,6,32);
histogram(DLE_final(2,6,:),100);
% axis off;
subplot(6,6,33);
histogram(DLE_final(3,6,:),100);
% axis off;
subplot(6,6,34);
histogram(DLE_final(4,6,:),100);
% axis off;
subplot(6,6,35);
histogram(DLE_final(5,6,:),100);
% axis off;
subplot(6,6,36);
histogram(DLE_final(6,6,:),100);
% axis off;
han=axes(fig,'visible','off'); 
han.Title.Visible='on';
han.XLabel.Visible='on';
han.YLabel.Visible='on';
ylabel(han,'Nombre sujets');
xlabel(han,'Nombre essais');
title(han,'DLE Source 246');

%%
%plot SD
figure(3)
SD_mean_form=flip(SD_mean'); 
set(axes,'FontSize',10);%,'position',[0.4686    0.10137    0.4059    0.2958]);%[0.5200    0.0619    0.3700    0.2700])%
colormap(gca,varycolor(256,'Moreland_Black_Body'))
hold on

imagesc(nsujs,ntris,SD_mean',[0.23 0.27])%,[0.03 0.13]
levs = [.275];
[xq,yq] = meshgrid(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)));

nutoplot = interp2(nsujs,ntris,SD_mean',xq,yq,'spline');
contour(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)),nutoplot,levs,'k','showtext','on')
set(gca,'layer','top')
box on
axis xy
xlabel('Number of subjects')
xticks(nsujs)
xticklabels(nsujs);
ylabel('Number of trials')
yticks(ntris)
yticklabels(ntris);%num2str(cellstr2num(get(gca,'yticklabel'))/2)
set(gca,'tag','power_contour')
h = colorbar;%('position',[0.9076    0.1037    0.0196    0.2958]);%,[0.9116    0.0619    0.0196    0.27]);
h.Label.String = 'SD';
h.Label.FontSize =12;

%set(gcf,'name',[num2str(i_ar,'%02d') ' ' strrep(all_areas.parcels{i_ar},'L_L_','')]);

%%
%histogramme

fig4 = figure(4);
clf

%2 trial
subplot(6,6,1);
histogram(SD_final(1,1,:),100); %2 sujets
%axis off;
subplot(6,6,2);
histogram(SD_final(2,1,:),100); %5 sujets
%axis off;
subplot(6,6,3);
histogram(SD_final(3,1,:),100);
%axis off;
subplot(6,6,4);
histogram(SD_final(4,1,:),100);
%axis off;
subplot(6,6,5);
histogram(SD_final(5,1,:),100);
%axis off;
subplot(6,6,6);
histogram(SD_final(6,1,:),100);
%axis off;

%5 trial
subplot(6,6,7);
histogram(SD_final(1,2,:),100);
%axis off;
subplot(6,6,8);
histogram(SD_final(2,2,:),100);
%axis off;
subplot(6,6,9);
histogram(SD_final(3,2,:),100);
%axis off;
subplot(6,6,10);
histogram(SD_final(4,2,:),100);
%axis off;
subplot(6,6,11);
histogram(SD_final(5,2,:),100);
%axis off;
subplot(6,6,12);
histogram(SD_final(6,2,:),100);
%axis off;

%10 tri
subplot(6,6,13);
histogram(SD_final(1,3,:),100);
% axis off;
subplot(6,6,14);
histogram(SD_final(2,3,:),100);
% axis off;
subplot(6,6,15);
histogram(SD_final(3,3,:),100);
% axis off;
subplot(6,6,16);
histogram(SD_final(4,3,:),100);
% axis off;
subplot(6,6,17);
histogram(SD_final(5,3,:),100);
% axis off;
subplot(6,6,18);
histogram(SD_final(6,3,:),100);
% axis off;

%20 tri
subplot(6,6,19);
histogram(SD_final(1,4,:),100);
% axis off;
subplot(6,6,20);
histogram(SD_final(2,4,:),100);
% axis off;
subplot(6,6,21);
histogram(SD_final(3,4,:),100);
% axis off;
subplot(6,6,22);
histogram(SD_final(4,4,:),100);
% axis off;
subplot(6,6,23);
histogram(SD_final(5,4,:),100);
% axis off;
subplot(6,6,24);
histogram(SD_final(6,4,:),100);
% axis off;

subplot(6,6,25);
histogram(SD_final(1,5,:),100);
% axis off;
subplot(6,6,26);
histogram(SD_final(2,5,:),100);
% axis off;
subplot(6,6,27);
histogram(SD_final(3,5,:),100);
% axis off;
subplot(6,6,28);
histogram(SD_final(4,5,:),100);
% axis off;
subplot(6,6,29);
histogram(SD_final(5,5,:),100);
% axis off;
subplot(6,6,30);
histogram(SD_final(6,5,:),100);
% axis off;

subplot(6,6,31);
histogram(SD_final(1,6,:),100);
% axis off;
subplot(6,6,32);
histogram(SD_final(2,6,:),100);
% axis off;
subplot(6,6,33);
histogram(SD_final(3,6,:),100);
% axis off;
subplot(6,6,34);
histogram(SD_final(4,6,:),100);
% axis off;
subplot(6,6,35);
histogram(SD_final(5,6,:),100);
% axis off;
subplot(6,6,36);
histogram(SD_final(6,6,:),100);
% axis off;
han=axes(fig4,'visible','off'); 
han.Title.Visible='on';
han.XLabel.Visible='on';
han.YLabel.Visible='on';
ylabel(han,'Nombre sujets');
xlabel(han,'Nombre essais');
title(han,'SD Source 246');


