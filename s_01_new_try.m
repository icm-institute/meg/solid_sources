%%
p_setpath


%%
fs = p_dbload('restin',{'name','label','ds','ctxsm','grad', 'hm'});
load(fullfile(datadir,'alldata_50timepoints.mat'))
alldata = reshape(alldata, [248,50*200,89]);
alldata = reshape(alldata, [248, 200, 50, 89]);
root_jobsdir = fullfile(jobsdir, '00_new_try');
mymkdir(root_jobsdir)


%% let's try to simulate 1 dipole
nchan = size(alldata,1);
ntimes = size(alldata,2);

sources = [487 658 246 4489 3776 1623];

nsujs = [2 10 30 50 70 88]; %nombre pair uniquement
nnsujs = numel(nsujs);
ntris = [2 5 10 20 30 50];    % total number of trials (all conditions) keep rem(ntris,2) == 0
nntris = numel(ntris);

all_specif = [];
all_specif(1).nMC = 500;
all_specif(1).times = 200;
all_specif(1).jobfilename = 's_00_new_try_jobs';
all_specif(1).mintime = -.2;
all_specif(1).maxtimes = 1;
all_specif(1).amp = [10];

for i = 1:numel(sources)
    all_specif(i) = all_specif(1);
    all_specif(i).jobsdir = fullfile(root_jobsdir,['source_' num2str(sources(i))]);
    
    all_specif(i).sources = sources(i);
end

 %%
% store head models, gradiometers and alldata
size(alldata)
all_cmd = {};

if not(exist(fullfile(root_jobsdir,'common.mat'),'file'))
    % saving common data across all simulations (loaded by each job)
    allctxsm = {fs.ctxsm};
    allgrad = {fs.grad};
    save(fullfile(root_jobsdir,'common.mat'),'allctxsm','allgrad','alldata','-v7.3');
end
%%
for i_spec = 1:numel(all_specif)
    
    specif = all_specif(i_spec);
    struct2ws(specif) % throw all fields of specif as variables in the local workspace
    scriptsdir = fullfile(root_jobsdir,'scripts/');
    
    %%
    
    allMCs = p_create_MCs(fs,nMC, nsujs, ntris);
    [allMCs.times] = rep2struct(times);
    
    fs_noctxsm = rmfield(fs,{'ctxsm','grad'});

    cfg = [];
    cfg.jobfilename = specif.jobfilename;
    cfg.jobnickname = 'BongoBlaster';
    cfg.scriptsdir = cd;
    cfg.scriptsdir2exclude = {'*.csv','*.fig','*.png','mctxsm*.mat','*.gif','fieldtrip','miscMatlab','solid_MEEG','Rcode','db','megconnectome*'};
    cfg.scriptsdir2link = {'fieldtrip' 'miscMatlab'};
    cfg.scriptsdir2linkhome = '/network/lustre/iss02/cenir/analyse/meeg/00_max/share';
    cfg.jobsdir = specif.jobsdir;
    cfg.vars2save = struct('fs',fs_noctxsm,'specif',specif);
    cfg.var2slice = struct('MCs',allMCs);
    cfg.slicedim = 3;
    cfg.sbatchcfg.mem = '8G';
    cfg.sbatchcfg.timelimit = '20:00:00';
    cfg.onlycommand = 0;
    cfg.doithere = 0;
    
    % the following line prepares independent jobs to be run on the cluster
    % fields of the cfg structure above are used to create in a destination
    % folder .jobsdir 
    %   - output, logs, errors subdirectories
    %   - a copy of local scripts stored in directory cfg.scriptsdir in a
    %       scripts subdirectory 
    %   - a clusterjob_common.mat file with data shared between
    %       jobs (vars2save).
    %   - for each job number ### to be run, 
    %       clusterjob###.mat file with unique variables necessary
    %           for the individual job (vars2slice). These variables are
    %           sliced along dimension slicedim, each job working with one
    %           slice.
    %       jobfilename###.m file with a simple script that cds to a target
    %           directory (scriptsdir) and runs jobfilename(###)
    %       jobfilename is a function that takes one numeric argument (###)
    %           and needs to be written elsewhere. 
    %   - a batch script for the cluster engine (SLURM in 2019 @ ICM) with
    %       a number of options (memory, number of cpus etc.)
    % see help of send2cluster
    all_cmd{i_spec} = p_send2cluster(cfg);

end

% now just run these lines in a terminal logged into the cluster server
cellfun(@disp,all_cmd);

% if you are not using slurm, your goal now is to run all of the
% p_figure02_jobs_###.m files independently.
