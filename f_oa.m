function [OA] = f_oa(pos,pow,i_source)
% compute dipole localisation error
% input : pos = position of all sources
%         pow = signal at all source positions
%         i_source = index of source of interest in pos and pow

%%
pos_i_source = pos(i_source, :);

n_j_source = size(pos,1);

del = isnan(pow);
pow(del) = 0;
Ai = 0;

    for j = 1:n_j_source

        %création de la variable Fij        
        Fij = mean(pow(j,:));
        
        %calcul Ai
        Ai = Ai + abs(Fij);
 
    end


%%
%calculate overall amplitude

OA = Ai;
return

end