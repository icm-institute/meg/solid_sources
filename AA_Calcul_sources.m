function Calcul_sources(ijob)

% for all sources of the the superhigh resolution mesh, take nsubj, ntri,
% compute a simple, inject signal, simple t-test, store result value

% preparing environment
%codedir = '/network/lustre/iss02/cenir/analyse/meeg/SOLID_SOURCES/jobs/00_first_try/amp_10/scripts';
%codedir = '/DATA_FAST/DATA/SOLID_SOURCES/jobs/00_new_try/amp_10/scripts';
codedir = cd;
addpath(codedir)
p_setpath

load('../../common.mat','allctxsm','alldata','allgrad');
load('../clusterjob_common.mat','fs','specif')
[fs.ctxsm] = rep2struct(allctxsm);
[fs.grad] = rep2struct(allgrad);

struct2ws(specif)

load(sprintf('../clusterjob%03d.mat',ijob),'MCs')

t_min = specif.mintime;
t_max = specif.maxtimes;
n_pts = specif.times;
%% start looping through sources

%for i_s = 5:size(fs(1).ctxsm.pos,1)
 for i_m = 1:length(specif.sources)
     i_s = specif.sources(i_m);
    %         tic
    if isfield(fs,'lf')
        fs = rmfield(fs,'lf');
    end
    %% load precomputed leadfield --> projecting this source to all channels
    load(fullfile(datadir,'ctxsm_all_lf_32k/','Native_32k_fs_LR',num2str(floor(i_s/1000),'%03d'),['lf_' num2str(i_s,'%06d')]),'lf');
    % now for each subject
    % we stack leadfields in the fs structure
    for isuj = 1:size(fs,1)
        % mark dipole position
        fs(isuj).dippos = fs(isuj).ctxsm.pos(i_s,:);
        fs(isuj).dipmom = fs(isuj).ctxsm.nrm(i_s,:)';
        fs(isuj).dipmom = fs(isuj).dipmom ./ norm(fs(isuj).dipmom);
%         size(alldata)
        all_cmd = {};
        
        if not(exist(fullfile(specif.jobsdir,'common.mat'),'file'))
            % saving common data across all simulations (loaded by each job)
            allctxsm = {fs.ctxsm};
            allgrad = {fs.grad};
            save(fullfile(specif.jobsdir,'common.mat'),'allctxsm','allgrad','alldata','-v7.3');
        end
        % take leadfield
        fs(isuj).lf = lf(:,:,isuj);
        if 0
            %% control plots
            %% individual source, dipole, and projection
            lftopo = fs(isuj).lf * fs(isuj).dipmom * specif.amp;
            %             lftopo =  lftopo+alldata(:,1,2,1);
            tmp = [];
            tmp.avg = lftopo;
            
            tmp.time = 1;
            tmp.label = fs(isuj).label;
            tmp.grad = fs(isuj).grad;
            tmp.dimord = 'chan_time';
            
            % projection to sensors
            figure(6868);clf;
            cfg = [];
            cfg.layout = '4D248.lay';
            cfg.zlim = [-4 4]*1e-13;%'maxabs';
            %             cfg.zlim = [-1e-13 1e-13];
            cfg.interactive = 'no';
            cfg.comment = 'no';
            cfg.markersize = 5;
            cfg.gridscale = 100;
            ft_topoplotER(cfg,tmp);
            colorbar
            
            % head and source model
            figure(3390);clf
            ft_plot_headmodel(fs(isuj).hm,'vertexcolor','none','facecolor','skin','edgecolor','none','facealpha',.5);
            hold on
            % source model (cortex)
            ft_plot_mesh(fs(isuj).ctxsm, 'facecolor', cortex_light)
            tmp = fs(isuj).ctxsm;
            tmpgrad = fs(1).grad;
            [sel,meglabels] = chnb(fs(1).label,tmpgrad.label);
            tmpgrad.label = tmpgrad.label(sel);
            tmpgrad.chanori = tmpgrad.chanori(sel,:);
            tmpgrad.chanpos = tmpgrad.chanpos(sel,:);
            tmpgrad.chantype = tmpgrad.chantype(sel);
            tmpgrad.chanunit = tmpgrad.chanunit(sel);
            tmpgrad.tra = tmpgrad.tra(sel,:);
            ft_plot_sens(tmpgrad)
            
            quiver3(fs(isuj).dippos(1),fs(isuj).dippos(2),fs(isuj).dippos(3),fs(isuj).dipmom(1)/10,fs(isuj).dipmom(2)/10,fs(isuj).dipmom(3)/10,'r','linewidth',5,'maxheadsize',2)
            lighting gouraud
            material shiny
            camlight
            %             view(140,20)
            rotate3d on
            title(sprintf('Sub %d source %d',isuj, i_s))
            drawnow
        end
    end
    if 0
        %% dipole projection
        for isuj = 1:size(fs,1)
            lftopo(:,:,isuj) = fs(isuj).lf * fs(isuj).dipmom * specif.amp;
        end
        lftopo = mean(lftopo,3);
        %             lftopo =  lftopo+alldata(:,1,2,1);
        tmp = [];
        tmp.avg = lftopo;
        
        tmp.time = 1;
        tmp.label = fs(isuj).label;
        tmp.grad = fs(isuj).grad;
        tmp.dimord = 'chan_time';
        
        % projection to sensors
        figure(6868);clf;
        cfg = [];
        cfg.layout = '4D248.lay';
        cfg.zlim = 'maxabs'; [-3.76 3.76]*1e-13;
        %             cfg.zlim = [-1e-13 1e-13];
        cfg.interactive = 'no';
        cfg.comment = 'no';
        cfg.markersize = 5;
        cfg.gridscale = 200;
        %         cfg.highlight = 'on';
        %         cfg.highlightchannel = maxsensor;
        ft_topoplotER(cfg,tmp);
        colorbar
        %% individual source, dipole
        
        % head and source model
        figure(3390);clf
        ft_plot_headmodel(fs(isuj).hm,'vertexcolor','none','facecolor','skin','edgecolor','none','facealpha',.5);
        hold on
        % source model (cortex)
        ft_plot_mesh(fs(isuj).ctxsm, 'facecolor', cortex_light)
        tmp = fs(isuj).ctxsm;
        tmpgrad = fs(1).grad;
        [sel,meglabels] = chnb(fs(1).label,tmpgrad.label);
        tmpgrad.label = tmpgrad.label(sel);
        tmpgrad.chanori = tmpgrad.chanori(sel,:);
        tmpgrad.chanpos = tmpgrad.chanpos(sel,:);
        tmpgrad.chantype = tmpgrad.chantype(sel);
        tmpgrad.chanunit = tmpgrad.chanunit(sel);
        tmpgrad.tra = tmpgrad.tra(sel,:);
        ft_plot_sens(tmpgrad)
        
        dippos = vertcat(fs.dippos);
        dipmom = horzcat(fs.dipmom)';
        quiver3(dippos(:,1),dippos(:,2),dippos(:,3),dipmom(:,1)/10,dipmom(:,2)/10,dipmom(:,3)/10,'r','linewidth',2,'maxheadsize',2,'autoscale','off')
        lighting gouraud
        material shiny
        camlight
        %             view(140,20)
        rotate3d on
        drawnow
    end
    
    nchan = size(alldata,1);
    ntimes = specif.times;%size(alldata,2);
    fprintf('#############  source%06d ###########\n',i_s)
    % now for all of the nMC data resamples
    for iMC = 1:numel(MCs)
        % find maxsensor across subjects of this MC
        clear alldipdat
        for i = 1:numel(MCs(iMC).sujs)
            alldipdat(:,:,i) = fs(MCs(iMC).sujs(i)).lf * fs(MCs(iMC).sujs(i)).dipmom * specif.amp;
        end
        [~,maxsensor] = maxabs(mean(alldipdat,3));
        % dimensions:
        % sensors, time, subjects;
        %         tic
        d1 = NaN([nchan,ntimes,size(MCs(iMC).tris,2)/2,numel(MCs(iMC).sujs)]);
        %         d2 = NaN(size(d1));
        % fill the data
        source_clean = [];
        for isuj = 1:numel(MCs(iMC).sujs)
            
            itri = 1:size(MCs(iMC).tris,2)/2;% first half of trials
            tmp = alldata(:,1:MCs(iMC).times,MCs(iMC).tris(isuj,itri),MCs(iMC).sujs(isuj));
            
            d1(:,:,:,isuj) = tmp;
            
            %             itri = (size(MCs(iMC).tris,2)/2+1):size(MCs(iMC).tris,2);% second half of trials
            %             tmp = alldata(:,1:MCs(iMC).times,MCs(iMC).tris(isuj,itri),MCs(iMC).sujs(isuj));
            %
            %             d2(:,:,:,isuj) = tmp;
            
            % C'est ici qu'on insère la reconstruction de sources
            if 0
                %%
                tmp = [];
                tmp.avg = alldipdat(:,:,isuj);
                tmp.time = 1;
                tmp.label = fs(isuj).label;
                tmp.grad = fs(MCs(iMC).sujs(isuj)).grad;
                tmp.dimord = 'chan_time';
                
                figure(6868);clf;
                cfg = [];
                cfg.layout = '4D248.lay';
                cfg.zlim = [-4e-14 4e-14];%'maxabs';
                cfg.highlight = 'on';
                cfg.highlightchannel = maxsensor;
                cfg.highlightsymbol = '.';
                cfg.highlightcolor = 'r';
                cfg.highlightsize = 20;
                ft_topoplotER(cfg,tmp);
                drawnow
            end
            data = [];
            data.label = fs(MCs(iMC).sujs(isuj)).label;
            data.grad = fs(MCs(iMC).sujs(isuj)).grad;
            
            
            for itri = 1:size(d1,3)
                data.trial{itri} = d1(:,:,itri,isuj);
                data.trial{itri}(:, 1:n_pts) = data.trial{itri}(:, 1:n_pts) + alldipdat(:,:,isuj);
                data.time{itri} = linspace(t_min, t_max, n_pts);
            end
            
            data.dimord = 'chan_time';
            %data.avg = lftopo1;
            
            
            cfg = [];
            cfg.covariance = 'yes';
            cfg.covariancewindow = [-.2 1];
            tlck = ft_timelockanalysis(cfg, data);
            
            % load leadfield for this subject
            load(fullfile(datadir,['lf_32k_reorg/lf_32k_' fs(MCs(iMC).sujs(isuj)).ds '.mat']))
            
            cfg               = [];
            cfg.method        = 'mne';
            cfg.sourcemodel   = fs(MCs(iMC).sujs(isuj)).ctxsm;
            cfg.sourcemodel.leadfield = lf;
            cfg.sourcemodel.inside = inside;
            cfg.headmodel     = fs(MCs(iMC).sujs(isuj)).hm;
            cfg.mne.prewhiten = 'yes';
            cfg.mne.lambda    = 3;
            cfg.mne.scalesourcecov = 'yes';
            
            source1{isuj} = ft_sourceanalysis(cfg,tlck);
            source_clean.pow(:,isuj) = mean(source1{isuj}.avg.pow,2);
            source_clean.pos(:,:,isuj) = source1{isuj}.pos;
            %source_clean.cov(:,:,isuj) = tlck.cov;
            
        end
        whosnan.pow = isnan(source_clean.pow);
        whosnan.pos = isnan(source_clean.pos);
        
        source_clean.pow = nanmean(source_clean.pow,2);
        source_clean.pos = nanmean(source_clean.pos,3);
        %source_clean.cov = nanmean(source_clean.cov,?);
        DLE(iMC) = f_dle(source_clean.pos,source_clean.pow, i_s);
        %if iMC=13 ... then DLE=1;
        SD(iMC) = f_sd(source_clean.pos,source_clean.pow, i_s);
    end
    %%
end


save(sprintf('../clusterjob%03d.mat',ijob),'MCs','DLE','SD','whosnan')


% clean_DLE = rmmissing(DLE);
% clean_SD = rmmissing(SD);
%
% DLE_mean = mean(clean_DLE);
% SD_mean = mean(clean_SD);
%
% DLE_std_h = DLE_mean + std(clean_DLE);
% DLE_std_l = DLE_mean - std(clean_DLE);
% DLE_Q = quantile(clean_DLE,3);
%
% SD_std_h = SD_mean + std(clean_SD);
% SD_std_l = SD_mean - std(clean_SD);
% SD_Q = quantile(clean_SD,3);
end