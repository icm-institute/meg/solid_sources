function [m] = ft_mesh_hemisphere(m,hemi)

% mesh = ft_mesh_hemisphere(mesh,hemi)
%
% set position of the other hemisphere to zero so as to display only hemi
% in subsequent ft_plot_mesh.
%
% input:
%   m = a mesh structure (fieldtrip format)
%   hemi = 1 or 2 = value to keep in the m.hemisphere
%

m.pos(m.hemisphere ~= hemi,:) = 0;
