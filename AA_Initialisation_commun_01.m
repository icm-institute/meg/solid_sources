%%
p_setpath


%%
%Recuperation et Initialisation des donnes 
fs = p_dbload('restin',{'name','label','headmodel','ds','atlas32','grad','hm'});

fs = renamefields(fs,'atlas32','ctxsm');
load(fullfile(datadir,'alldata_50timepoints.mat'))

%Initialisation du chemin de stockage
date = datestr(now, 'dd-mm-HHMM');%Pour versionning et s'y retrouver
nom_fichier =   ['JobsMargaux_01'];
root_jobsdir = fullfile(jobsdir, nom_fichier);
mymkdir(root_jobsdir)

%% let's try to simulate 1 dipole
nchan = size(alldata,1);
ntimes = size(alldata,2);%Nbre de personnes ?

amps = [10];%Une amplitude (defini arbitrairement?)

% nombre de sujets utilisés pour les différentes simus
nsujs = [5:5:50];   %chanter le 5 par le 10
nnsujs = numel(nsujs);
% nombre d'essais utilisés pour les simus
ntris = [2 20:20:200];    % total number of trials (all conditions) keep rem(ntris,2) == 0
%Changer le 20 par le 40
%Nbre d'essais par personnes
nntris = numel(ntris);

all_specif = [];
all_specif(1).nMC = 300; % nombre de simus pour chaque "case" de notre figure finale ==> nombre de jobs
all_specif(1).times = 50;
all_specif(1).mintime = -.2;
all_specif(1).maxtimes = 1;

all_specif(1).jobfilename = 'AA_Calcul_sources';

all_specif(1) = all_specif(1);
all_specif(1).jobsdir = fullfile(root_jobsdir,'essai_02');
all_specif(1).sources = [
    5579
    5276
    7941
    7248
    ];

all_specif(1).amp = amps(1);

%%
% store head models, gradiometers and alldata
size(alldata)
all_cmd = {};

if not(exist(fullfile(root_jobsdir,'common.mat'),'file'))
    % saving common data across all simulations (loaded by each job)
    allctxsm = {fs.ctxsm};
    allgrad = {fs.grad};
    save(fullfile(root_jobsdir,'common.mat'),'allctxsm','allgrad','alldata','-v7.3');
end

%%
for i_spec = 1:numel(all_specif)
    
    
    specif = all_specif(i_spec);
    struct2ws(specif) % throw all fields of specif as variables in the local workspace
    scriptsdir = fullfile(root_jobsdir,'scripts/');
    
    %%
    
    allMCs = p_create_MCs(fs,nMC, nsujs, ntris);
    [allMCs.times] = rep2struct(times);
    
    fs_noctxsm = rmfield(fs,{'ctxsm','grad'});

    cfg = [];
    cfg.jobfilename = specif.jobfilename;
    cfg.jobnickname = 'BongoBlaster';
    cfg.scriptsdir = codedir;
    cfg.scriptsdir2exclude = {'Subject01*', '*.csv','*.fig','*.png','mctxsm*.mat','*.gif','fieldtrip','miscMatlab','solid_MEEG','Rcode','db','megconnectome*'};
    cfg.scriptsdir2link = {'fieldtrip' 'miscMatlab'};
    cfg.scriptsdir2linkhome = '/network/lustre/iss02/cenir/analyse/meeg/00_max/share';
    cfg.jobsdir = specif.jobsdir;
    cfg.vars2save = struct('fs',fs_noctxsm,'specif',specif);
    cfg.var2slice = struct('MCs',allMCs);
    cfg.slicedim = 3;
    cfg.sbatchcfg.mem = '8G';
    cfg.sbatchcfg.timelimit = '20:00:00';
    cfg.onlycommand = 0;
    cfg.doithere = 1;
    
    % the following line prepares independent jobs to be run on the cluster
    % fields of the cfg structure above are used to create in a destination
    % folder .jobsdir 
    %   - output, logs, errors subdirectories
    %   - a copy of local scripts stored in directory cfg.scriptsdir in a
    %       scripts subdirectory 
    %   - a clusterjob_common.mat file with data shared between
    %       jobs (vars2save).
    %   - for each job number ### to be run, 
    %       clusterjob###.mat file with unique variables necessary
    %           for the individual job (vars2slice). These variables are
    %           sliced along dimension slicedim, each job working with one
    %           slice.
    %       jobfilename###.m file with a simple script that cds to a target
    %           directory (scriptsdir) and runs jobfilename(###)
    %       jobfilename is a function that takes one numeric argument (###)
    %           and needs to be written elsewhere. 
    %   - a batch script for the cluster engine (SLURM in 2019 @ ICM) with
    %       a number of options (memory, number of cpus etc.)
    % see help of send2cluster
    all_cmd{i_spec} = p_send2cluster(cfg);

end