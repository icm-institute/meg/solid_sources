
%%load variables of interest from clusterjob files into workspace


%load all .mat files starting with clusterjob
%clusterjob_dir = '/DATA_FAST/DATA/SOLID_SOURCES/jobs/00_new_try/amp_10/';
clusterjob_dir = '/network/lustre/iss02/cenir/analyse/meeg/SOLID_SOURCES/jobs/00_new_try/source_4489/';
clusterjob_pattern = fullfile(clusterjob_dir, 'clusterjob*.mat');

clusterjob_files = dir(clusterjob_pattern);
%%
%delete last clusterjob file which corresponds to clusterjob_common file
%clusterjob_files(regexpcell({clusterjob_files.name},'common')) = [];
i_clusterjob_common = numel(clusterjob_files);
clusterjob_files(i_clusterjob_common)=[];
%%
%extract variables from .mat files
for k = 1:length(clusterjob_files)
  clusterjob_filename = fullfile(clusterjob_dir, clusterjob_files(k).name);
  clusterjob_data = load(clusterjob_filename); % Retrieves a structure.
  DLE_raw(k,:) = clusterjob_data.DLE;
  SD_raw(k,:) = clusterjob_data.SD;
end


MCs_raw = clusterjob_data.MCs;

%trouver nsujs
for i = 1:size(MCs_raw,2)
    numsuj = size(MCs_raw(i, 1).sujs,2);
    nsujs(i) = numsuj;
end

%trouver ntri
for j = 1:size(MCs_raw,1)
    numtri = size(MCs_raw(1, j).tris,2);
    ntris(j) = numtri;
end

nnsujs = numel(nsujs);
nntris = numel(ntris);

%%
%reshape

DLE_final = reshape(DLE_raw',[6,6,500]);
SD_final = reshape(SD_raw',[6,6,500]);
%%
%stat

DLE_mean = mean(DLE_final,3);
SD_mean = mean(SD_final,3);

DLE_std_h = DLE_mean + std(DLE_final);
DLE_std_l = DLE_mean - std(DLE_final);
DLE_Q = quantile(DLE_final,3);

SD_std_h = SD_mean + std(SD_final);
SD_std_l = SD_mean - std(SD_final);
SD_Q = quantile(SD_final,3);

[min_DLE i_min_DLE] = min(DLE_final);
[min_SD i_min_SD] = min(SD_final);
[max_DLE i_max_DLE] = max(DLE_final);
[max_SD i_max_SD] = max(SD_final);

%% 
%plot

 
set(axes,'FontSize',10);%,'position',[0.4686    0.10137    0.4059    0.2958]);%[0.5200    0.0619    0.3700    0.2700])%
colormap(gca,varycolor(256,'Moreland_Black_Body'))
hold on

imagesc(nsujs,ntris,DLE_mean,[0.03 0.13])
levs = [.06 .08 .1];
[xq,yq] = meshgrid(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)));

nutoplot = interp2(nsujs,ntris,DLE_mean,xq,yq,'spline');
contour(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)),nutoplot,levs,'k','showtext','on')
set(gca,'layer','top')
box on
axis xy
xlabel('Number of subjects')
xticks(nsujs)
xticklabels(nsujs);
ylabel('Number of trials')
yticks(ntris)
yticklabels(num2str(cellstr2num(get(gca,'yticklabel'))/2));
set(gca,'tag','power_contour')
h = colorbar;%('position',[0.9076    0.1037    0.0196    0.2958]);%,[0.9116    0.0619    0.0196    0.27]);
h.Label.String = 'DLE';
h.Label.FontSize =12;

%set(gcf,'name',[num2str(i_ar,'%02d') ' ' strrep(all_areas.parcels{i_ar},'L_L_','')]);


