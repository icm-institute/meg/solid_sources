

function output_txt = test_atlas_cb(~,info)
% Display the position of the data cursor
% obj          Currently not used (empty)
% event_obj    Handle to event object
% output_txt   Data cursor text string (string or cell array of strings).

if not(exist('dodel','var'))
    if isobject(obj)
        dodel = 1;
    else
        dodel = 0;
    end
end
n = get(findobj(gca,'-regexp','tag','dip\d+'),'tag');
if isempty(n)
    n = 1;
else
    n = max(cellfun(@(x)str2num(x(4:end)),unique(n))) + 1;
end

tmp = get(gcf,'userdata');
ctxsm = tmp{1};vtx = tmp{2};
if not(isfield(ctxsm,'areas'))
    ctxsm.areas.parcels = {};
    ctxsm.areas.coords = [];
end
try
    alt = 0;
    set(obj,'HandleVisibility','on');
    %     if isempty(get(obj,'tag'))
    %         n = numel(findobj(gcf,'type','hggroup'));
    %         set(obj,'tag',['datatip' num2str(n)]);
    %     end
    %     n = get(obj,'tag');n = str2num(n(8));
    pos = get(event_obj,'Position');
    output_txt = {['X: ',num2str(pos(1),4)],...
        ['Y: ',num2str(pos(2),4)]};
    % If there is a Z-coordinate in the position, display it as well
    if length(pos) > 2
        output_txt{end+1} = ['Z: ',num2str(pos(3),4)];
    end
catch
    output_txt = {''};
    idx = event_obj;
    pos = ctxsm.pos(idx,:);
end

try
    output_txt(end+1) = ctxsm.parcellationlabel(ctxsm.parcellation(dsearchn(ctxsm.pos,pos)));
    ctxsm.areas.parcels = [ctxsm.areas.parcels output_txt(end)];
    ctxsm.areas.coords = [ctxsm.areas.coords; pos(1:3)];
    if not(isfield(ctxsm,'nrm'))
        % compute normals
        ctxsm.nrm = normals(ctxsm.pos, ctxsm.tri);
        set(gcf,'userdata',{ctxsm vtx});
    end
    idx = dsearchn(ctxsm.pos,pos);
    dippos = ctxsm.pos(idx,:);
    adippos = ctxsm.allpos(idx,:,:);
    dipmom = ctxsm.nrm(idx,:);
    adipmom = ctxsm.allnrm(idx,:,:);
    output_txt{end+1} = num2str(idx);
    ax = {'top','left','med','back','bottom'};
    for i = 1:numel(ax)
        set(gcf,'CurrentAxes',findobj(gcf,'tag',ax{i}))
        hold on
        try
            if dodel
                delete(findobj(gca,'-regexp','tag',['dip\d+']))
            end
        end
        h = quiver3(dippos(1),dippos(2),dippos(3),dipmom(1),dipmom(2),dipmom(3),.05,'r','linewidth',2,'maxheadsize',1);
        set(h,'tag',['dip' num2str(n)])
        h = quiver3(repmat(dippos(1),size(adippos(1,1,:))),repmat(dippos(2),size(adippos(1,2,:))),repmat(dippos(3),size(adippos(1,3,:))),adipmom(1,1,:),adipmom(1,2,:),adipmom(1,3,:),.02,'k','linewidth',1,'maxheadsize',.5,'HitTest','on');
        %         h = quiver3(adippos(1,1,:),adippos(1,2,:),adippos(1,3,:),adipmom(1,1,:),adipmom(1,2,:),adipmom(1,3,:),5,'k','linewidth',1,'maxheadsize',.5);
        set(h,'tag',['dip' num2str(n)])
    end
    output_txt{end+1} = num2str(vtx(idx));
end
todisp = regexprep(output_txt,'[XYZ]: ','');
todisp = sprintf('%s    ',todisp{:});
disp(todisp)


end