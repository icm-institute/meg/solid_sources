% figures 6 10 14 18 share the same layout.
% this scripts creates them all
p_setpath

fs = p_dbload('restin',{'name','label','ds','grad','hm','transform'});

p_neighb

root_jobsdir = fullfile(jobsdir,'JobsMargaux_01/essai_01');
figdir = fullfile(figsdir,'Figure_test_01');mymkdir(figdir)

load(fullfile(datadir,'alldata_50timepoints.mat'))

%%

% number of resamples
nMC = 300;

% benchmark of subjects and trials
nsujs = 5:5:50;
nnsujs = numel(nsujs);
ntris = [2 20:20:200];    % keep rem(ntris,2) == 0
nntris = numel(ntris);

dipamp = 5;

p_all_areas
i_ar = 4;
seed = all_areas.seeds(i_ar);
fs = p_dbload('restin',{'name','label','headmodel','ds','ctxsm','grad','hm'});
mctxsm = p_create_meanctx('4',fs);

d2 = NaN(size(mctxsm.pos, 1), 1);
for ipos = 1:size(mctxsm.pos, 1)
    distances = NaN(size(fs, 1), 1);
    for isuj = 1:size(fs, 1)
        chanpos = fs(isuj).grad.chanpos;
        distances(isuj) = min(sqrt(sum((mctxsm.pos(ipos, :) - chanpos).^2, 2)));
    end
    d2(ipos) = mean(distances);
end
mctxsm.closestchan = d2 * 100;

[sm_specs,initial] = figure_source_specs(mctxsm);


%% collect responses

disp(root_jobsdir)
perf = cell(numel(sm_specs),1);
for ispec = 1:numel(sm_specs)
    % gather results
    [MCs, whosnan, DLE, SD] = p_gather_clusterjobs(fullfile(root_jobsdir));
    DLE = reshape(DLE, size(MCs));
    SD = reshape(SD, size(MCs));
    
    booboots = struct2table(MCs(:));
    booboots.sujs = cellfun(@numel,booboots.sujs);
    booboots.tris = cellfun(@(x) size(x,2),booboots.tris);
    booboots.name = repmat(sm_specs(ispec).name,size(booboots,1),1);
    booboots.amp = repmat(dipamp,size(booboots,1),1);
    booboots.ispec = repmat(ispec,size(booboots,1),1);
    booboots.DLE = DLE(:);
    booboots.SD = SD(:);
    
    
end
% moyenner booboots en groupant par nombre de sujets et nombre d'essais
% utilise grpstats
% Grouper par le nombre de sujets et le nombre d'essais, puis calcul des moyennes
results = grpstats(booboots, {'sujs', 'tris'}, {'mean'}, 'DataVars', {'DLE'});

% Extraire les moyennes de DLE, le nombre de sujets et le nombre d'essais
mean_DLE = results.mean_DLE;
sujs = results.sujs;
tris = results.tris;

% Déterminer les valeurs uniques de sujets et d'essais
unique_sujs = unique(sujs);
unique_tris = unique(tris);

% Créer une matrice pour stocker les moyennes de DLE correspondantes
DLE_matrix = NaN(length(unique_sujs), length(unique_tris));

% Remplir la matrice avec les moyennes de DLE
for i = 1:length(unique_sujs)
    for j = 1:length(unique_tris)
        % Trouver l'index correspondant dans les moyennes
        idx = sujs == unique_sujs(i) & tris == unique_tris(j);
        if any(idx)
            DLE_matrix(i, j) = mean_DLE(idx);
        end
    end
end


% Créer le graphique en utilisant imagesc
figure;

imagesc(DLE_matrix);
axis xy
colorbar; % Ajouter une barre de couleur pour visualiser les valeurs

% Définir les étiquettes d'axe
set(gca, 'XTick', 1:length(unique_tris), 'XTickLabel', unique_tris);
set(gca, 'YTick', 1:length(unique_sujs), 'YTickLabel', unique_sujs);

xlabel('Nombre d''essais');
ylabel('Nombre de sujets');
title('Moyenne de DLE par nombre de sujets et nombre d''essais');
%% power plot

%%
% tmp_sm_specs = sm_specs(regexpcell({sm_specs.name},props{iprop}));
figure;
set(gca,'FontSize',14);
colormap(gca,varycolor(256,'Moreland_Black_Body'))
hold on
% récupérer les valeurs de DLE dans booboots
toplot = reshape(results,nntris,nnsujs);
[]
imagesc(nsujs,ntris,toplot,[0 1])
levs = [.1 .5 .8];
[xq,yq] = meshgrid(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)));
nutoplot = interp2(nsujs,ntris,toplot,xq,yq,'spline');
contour(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)),nutoplot,levs,'k','showtext','on')
set(gca,'layer','top')
box on
axis xy

xticks(nsujs)
xticklabels(nsujs);
set(gca,'XTickLabelRotation',90)
figure;
set(gca,'FontSize',14);
colormap(gca,varycolor(256,'Moreland_Black_Body'))
hold on
% récupérer les valeurs de DLE dans booboots
toplot = reshape(results,nntris,nnsujs);
[]
imagesc(nsujs,ntris,toplot,[0 1])
levs = [.1 .5 .8];
[xq,yq] = meshgrid(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)));
nutoplot = interp2(nsujs,ntris,toplot,xq,yq,'spline');
contour(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)),nutoplot,levs,'k','showtext','on')
set(gca,'layer','top')
box on
axis xy

xticks(nsujs)
xticklabels(nsujs);
set(gca,'XTickLabelRotation',90)
yticks(ntris)
yticklabels(num2str(cellstr2num(get(gca,'yticklabel'))/2));
if ispec == 1
    
    xlabel('Number of subjects')
    ylabel('Number of trials')
end
set(gca,'tag','power_contour')

if ispec == numel(tmp_sm_specs)
    h = colorbar('Position',[0.9291    0.1841    0.0152    0.7292]);
    h.Label.String = 'Power';
    h.Label.FontSize =16;
end


%%
set(gcf,'paperpositionmode','auto')
drawnow

print('-dpng','-r300',fullfile(figdir,['PowerPlot_' props{iprop} '.png']))






yticks(ntris)
yticklabels(num2str(cellstr2num(get(gca,'yticklabel'))/2));
if ispec == 1
    
    xlabel('Number of subjects')
    ylabel('Number of trials')
end
set(gca,'tag','power_contour')

if ispec == numel(tmp_sm_specs)
    h = colorbar('Position',[0.9291    0.1841    0.0152    0.7292]);
    h.Label.String = 'Power';
    h.Label.FontSize =16;
end

%%
set(gcf,'paperpositionmode','auto')
drawnow

print('-dpng','-r300',fullfile(figdir,['PowerPlot_' props{iprop} '.png']))





