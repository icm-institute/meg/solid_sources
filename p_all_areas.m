% area names, coordinates, corresponding vertex number (seed) and some
% viewing params for figures
% all from the 32k surfaces


all_areas.parcels = {
    'L_L_G_front_sup'
    'L_L_G_precentral'
    'L_L_S_temporal_sup'
    'L_L_G_occipital_sup'
    'L_L_Pole_occipital'
    'L_L_G_Ins_lg_and_S_cent_ins'
    'L_L_G_oc-temp_lat-fusifor'
    'L_L_G_orbital'
    'L_L_G_precentral'
    'L_L_G_orbital'
    'L_L_G_postcentral'
    'L_L_S_front_middle'
    'L_L_G_occipital_sup'
    'L_L_G_occipital_sup'
    'L_L_G_postcentral'
    };
all_areas.coords = [
    0.06541    0.009143    0.06354
    -0.004398    0.03671    0.07827
    -0.04657    0.04207    0.03011
    -0.06911    0.01164    0.04795
    -0.08084    0.01645    0.0118
    0.005745    0.03639    0.0302
    -0.03874    0.03302    0.004845
    0.03705    0.02576    0.008244
    -0.01207    0.03342    0.07031
    0.04372    0.01272    0.006652
    -0.006737    0.04846    0.06961
    0.07146    0.02145    0.04102
    -0.07809    0.01532    0.03088
    -0.06933    0.01978    0.04862
    -0.02167    0.02301    0.08272
    ];
all_areas.seeds = [
    29096
    5579
    15834
    12496
    23870
    10483
    21567
    20338
    5276
    20861
    7941
    28386
    24842
    12323
    7248
    ];

all_areas.view = [-90 90
    -90 90
    -180 0
    -90 90
    -90 90
    -180 0
    -180 0
    -180 0
    -90 90
    -180 0
    -90 90
    -90 90
    -90 90
    -90 90
    -90 90
    
    ];
all_areas.topoclim = {[-40 40]
    [-40 40]
    [-40 40]
    [-40 40]
    [-40 40]
    [-40 40]
    [-40 40]
    [-40 40]
    [-40 40]
    [-40 40]
    [-40 40]
    [-40 40]
    [-40 40]
    [-40 40]
    [-40 40]
    };