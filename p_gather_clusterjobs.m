function varargout = p_gather_clusterjobs(jobsdir)

% load results of cluster jobs found in jobsdir
% or load results listed in structure jobsdir.name

if ~isstruct(jobsdir)
    fs = flister('clusterjob(?<ijob>\d+)\.mat','dir',jobsdir,'recurse',0);
    [~,idx] = sort(cellfun(@str2num,{fs.ijob}));
    fs = fs(idx);
else
    fs = jobsdir;
end
njobs = fs(end).ijob;
textprogressbar()
textprogressbar('loading jobs data...')
for i_f = 1:numel(fs)
    textprogressbar(i_f/numel(fs)*100);
    d = load(fs(i_f).name);
    fn = fieldnames(d);
    for i = 1:numel(fn)
        n = ndims(d.(fn{i}));
        str = [fn{i} '(' repmat(':,',1,n)];
        str = [str num2str(i_f) ')'];
        str = [str ' = d.' fn{i} ';'];
        try
            eval(str);
        catch
            error(['Problem with ' fs(i_f).name])
        end
    end
end
for i = 1:numel(fn)
    eval(['varargout{i} = ' fn{i} ';']);
end
textprogressbar('')

    